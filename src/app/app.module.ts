import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { HeaderComponent } from './components/header/header.component';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { InvUploadSessionComponent } from './components/inv-upload-session/inv-upload-session.component';
import { HTTP_INTERCEPTORS, HttpClient, HttpClientModule } from '@angular/common/http';
import { ApiService } from './api_services/api.service';
import { LoginComponent } from "./components/login/login.component";
import {  ReactiveFormsModule, FormControl } from "@angular/forms";
import { AuthInterceptor } from './api_services/auth.interceptor';


@NgModule({
    declarations: [
        AppComponent,
        HeaderComponent,
        InvUploadSessionComponent,
       
        
    ],
    providers: [HttpClient, ApiService, {
        provide : HTTP_INTERCEPTORS,
        useClass: AuthInterceptor,
        multi : true

    }],
    bootstrap: [AppComponent],
    imports: [
        BrowserModule,
        AppRoutingModule,
        NgbModule,
        HttpClientModule,
        LoginComponent,
        ReactiveFormsModule,        
    ]
})
export class AppModule { }

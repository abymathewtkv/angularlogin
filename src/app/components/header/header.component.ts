import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.css']
})
export class HeaderComponent implements OnInit {

  @Input() childVariable : string = '' 

  name = 'This is for parent from child';

  @Output() updatedName = new EventEmitter<string>();

  ngOnInit(): void {
    this.updatedName.emit(this.name)
  }
  


}

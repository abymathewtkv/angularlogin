import { Component, importProvidersFrom, inject,OnInit } from '@angular/core';
import { ApiService } from 'src/app/api_services/api.service';
import { NgbCalendar,NgbDatepickerModule,NgbDateStruct } from "@ng-bootstrap/ng-bootstrap";
import { catchError } from 'rxjs';





@Component({
  selector: 'app-inv-upload-session',
  templateUrl: './inv-upload-session.component.html',
  styleUrls: ['./inv-upload-session.component.css'],
 
})
export class InvUploadSessionComponent implements OnInit  {


  private response: any;

  public errorMessage = '';

  public xmlData = '';

  public uploadResponse: any;
  

  constructor(private apiService :ApiService ){}

  ngOnInit(): void {
    

    
  }


  getParcelType(){

    this.apiService.getInVoiceParcelType().subscribe(
      (data)=> this.response = data, error=>this.errorMessage = error
     
    )

    if(this.response.Status == true){

      this.xmlData = this.response.Data;
     
    }


    console.log(this.response);

   
  }

  clearXMlData(){

    this.xmlData = '';
  }



  uploadInvoice(){

    this.apiService.uploadInvoice().subscribe(
      (data)=> this.uploadResponse = data, (error)=>this.errorMessage = error);
      
    
  }


  



}

import { ComponentFixture, TestBed } from '@angular/core/testing';

import { InvUploadSessionComponent } from './inv-upload-session.component';

describe('InvUploadSessionComponent', () => {
  let component: InvUploadSessionComponent;
  let fixture: ComponentFixture<InvUploadSessionComponent>;

  beforeEach(() => {
    TestBed.configureTestingModule({
      declarations: [InvUploadSessionComponent]
    });
    fixture = TestBed.createComponent(InvUploadSessionComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

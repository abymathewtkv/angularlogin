import { HttpClient } from '@angular/common/http';
import { Component, inject } from '@angular/core';
import { FormBuilder, FormControl, FormGroup, FormsModule, Validators,ReactiveFormsModule } from '@angular/forms';
import { Router } from '@angular/router';
import { AuthService } from 'src/app/api_services/auth.service';


@Component({
  selector: 'app-login',
  standalone: true,
  imports: [FormsModule, ReactiveFormsModule ],
  templateUrl: './login.component.html',
  styleUrl: './login.component.css'
})
export class LoginComponent {
  username: string = '';
  password: string = '';

  loginResponse: any

//  loginForm: FormGroup;
  

  constructor(private authservice: AuthService, private route :Router) {

    // this.loginForm = this.fb.group(
    //   {
    //     username: ['', Validators.required],
    //     password: ['', Validators.required]
    //   }
    // )

    
  }



  onSubmitLoginForm() {

    const loginObj = {
      "email": this.username,
      "password": this.password
    }
  
     if(loginObj.email != '' && loginObj.password != ''){

      this.authservice.loginService(loginObj).subscribe(
        (data: any) => this.loginResponse = data
  
      )

      if(this.loginResponse.access_token != ''){

        localStorage.setItem('token',this.loginResponse.access_token)

        this.route.navigateByUrl('dashBoard')
      } else {
        
        alert("invalid credential");
      }

     } else {

      alert("one of the field is empty");
     }

  





    console.log(this.loginResponse);


  }





}

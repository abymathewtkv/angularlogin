import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable, catchError, throwError } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class ApiService {

   private apiUrl = "https://uatapialbhub.accuratelegalbilling.com/api";

   private url = "https://jsonplaceholder.org/posts"; 

  constructor(private http : HttpClient) { 
    
  }

  getInVoiceParcelType(){


//    var headers = new HttpHeaders({

//      'Content-Type': 'application/json',
//      'Access-Control-Allow-Origin' : "true"
  
    
//     }
// )
    return this.http.get(this.apiUrl+"/ALBHubAPI/InvoiceParcelType", );
    // .pipe(

      
    //   catchError(error => {
    //     console.error('Error:', error);
    //     return throwError(error);
    //   })
    // );

  }


  uploadInvoice(){


    var header = new HttpHeaders({

      "Content-Type" : "application/json",
      "Access-Control-Allow-Origin": 'http://localhost:4200'
    });

  
    return this.http.post(this.apiUrl+"/ALBHubAPI/UploadInvoice",{}, {headers : header});
  }



  
}

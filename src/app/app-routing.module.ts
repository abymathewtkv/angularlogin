import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { LoginComponent } from './components/login/login.component';
import { InvUploadSessionComponent } from './components/inv-upload-session/inv-upload-session.component';

const routes: Routes = [

  { path: 'login', component: LoginComponent },
  { path: 'dashBoard', component: InvUploadSessionComponent },
  { path: '', redirectTo: "login",pathMatch : 'full' },
  { path: '**', component:  LoginComponent  }

  


];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule {  }
